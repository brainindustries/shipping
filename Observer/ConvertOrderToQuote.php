<?php

namespace Shirtplatform\Shipping\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class ConvertOrderToQuote
 * @package Shirtplatform\Shipping\Observer
 */
class ConvertOrderToQuote implements ObserverInterface
{

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getOrder();
        $quote = $observer->getQuote();
        $quote->setDeliveryBranchId($order->getDeliveryBranchId());
        $quote->setDeliveryBranchName($order->getDeliveryBranchName());
    }

}
