<?php

namespace Shirtplatform\Shipping\Observer;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\OfflinePayments\Model\Cashondelivery;
use Magento\Store\Model\ScopeInterface;

class CashOnDeliveryCheck implements ObserverInterface
{

    /**
     * @var ScopeConfigInterface
     */
    private $_scopeConfig;

    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->_scopeConfig = $scopeConfig;
    }

    public function execute(Observer $observer)
    {
        if ($observer->getQuote() == null || $observer->getQuote()->isVirtual()) {
            return;
        }

        $shippingMethod = $observer->getQuote()->getShippingAddress()->getShippingMethod();
        $paymentMethod = $observer->getMethodInstance()->getCode();

        $checkedMethods = [
            'shirtplatform_basic_shirtplatform_basic',
            'shirtplatform_standard_shirtplatform_standard',
            'shirtplatform_premium_shirtplatform_premium',
            'shirtplatform_express_shirtplatform_express'            
        ];

        if (
            $paymentMethod == Cashondelivery::PAYMENT_METHOD_CASHONDELIVERY_CODE &&
            in_array($shippingMethod, $checkedMethods)
        ) {
            $shippingRates = $observer->getQuote()->getShippingAddress()->getAllShippingRates();

            foreach ($shippingRates as $rate) {
                if (
                    $rate->getCode() == $shippingMethod &&
                    $this->_scopeConfig->getValue('carriers/' . $rate->getCarrier() . '/hide_cashondelivery', ScopeInterface::SCOPE_STORE, $observer->getQuote()->getStoreId())
                ) {
                    $checkResult = $observer->getResult();
                    $checkResult->setData('is_available', false);
                }
            }
        }
    }
}
