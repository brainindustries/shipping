<?php

namespace Shirtplatform\Shipping\Observer;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class AddHtmlToOrderShippingViewObserver
 * @package Shirtplatform\Shipping\Observer
 */
class AddHtmlToOrderShippingView implements ObserverInterface
{

    /** @var \Magento\Framework\View\Element\TemplateFactory */
    private $templateFactory;

    /** @var ScopeConfigInterface */
    private $scopeConfig;

    /**
     * AddHtmlToOrderShippingView constructor.
     * @param \Magento\Framework\View\Element\TemplateFactory $templateFactory
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\View\Element\TemplateFactory $templateFactory,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->templateFactory = $templateFactory;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if($observer->getElementName() === 'order_shipping_view')
        {
            $orderShippingViewBlock = $observer->getLayout()->getBlock($observer->getElementName());
            $order = $orderShippingViewBlock->getOrder();

            $customBlock = $this->templateFactory->create();
            $customBlock->setDeliveryBranchId($order->getDeliveryBranchId());
            $customBlock->setDeliveryBranchName($order->getDeliveryBranchName());
            $customBlock->setTemplate('Shirtplatform_Shipping::order_info_shipping_info.phtml');

            $html = $observer->getTransport()->getOutput() . $customBlock->toHtml();
            $observer->getTransport()->setOutput($html);
        }
        elseif($observer->getElementName() === 'shipping_method')
        {
            $orderShippingViewBlock = $observer->getLayout()->getBlock($observer->getElementName());
            $quote = $orderShippingViewBlock->getQuote();

            $customBlock = $this->templateFactory->create();
            $customBlock->setQuote($quote);
            $customBlock->setDeliveryBranchId(($quote->getDeliveryBranchId() != null) ? $quote->getDeliveryBranchId() : 0);
            $customBlock->setDeliveryBranchName($quote->getDeliveryBranchName());            
            $customBlock->setShippingMethod(($quote->getShippingAddress() != null) ? $quote->getShippingAddress()->getShippingMethod() : null);
            $customBlock->setTemplate('Shirtplatform_Shipping::order_edit_shipping_info.phtml');

            $html = $observer->getTransport()->getOutput() . $customBlock->toHtml();
            $observer->getTransport()->setOutput($html);
        }
    }

}
