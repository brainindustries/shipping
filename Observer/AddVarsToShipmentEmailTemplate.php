<?php

namespace Shirtplatform\Shipping\Observer;

use Magento\Framework\Event\ObserverInterface;

class AddVarsToShipmentEmailTemplate implements ObserverInterface {
    
    /**
     * @var \Shirtplatform\Shipping\Helper\Data
     */
    private $_shippingHelper;
    
    /**
     * 
     * @param \Shirtplatform\Shipping\Helper\Data $shippingHelper
     */
    public function __construct(\Shirtplatform\Shipping\Helper\Data $shippingHelper) {
        $this->_shippingHelper = $shippingHelper;
    }
    
    /**
     * Add carrier to shipment email template
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {        
        $transportObject = $observer->getData('transportObject');
        $shipment = $transportObject->getShipment();

        if ($shipment->getProviderId()) {
            $carrier = $this->_shippingHelper->getModuleProviderById($shipment->getProviderId(), $shipment->getStoreId());
            
            if ($carrier) {
                $transportObject->setCarrier($carrier->name);
            }
        }
    }
}