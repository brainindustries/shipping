<?php
namespace Shirtplatform\Shipping\Observer;


use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class SalesOrderCreateProcessDataBefore
 * @package Shirtplatform\Shipping\Observer
 */
class SalesOrderCreateProcessDataBefore implements ObserverInterface
{

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $data = $observer->getRequestModel()->getPost('order');
        if($data && isset($data['shipping_delivery_branch_id']))
        {
            $orderCreateModel = $observer->getOrderCreateModel();
            $quote = $orderCreateModel->getQuote();
            $quote->setDeliveryBranchId(($data['shipping_delivery_branch_id'] != null) ? $data['shipping_delivery_branch_id'] : null);
            $quote->setDeliveryBranchName(($data['shipping_delivery_branch_name'] != null)  ? $data['shipping_delivery_branch_name'] : null);
        }
    }

}
