<?php

namespace Shirtplatform\Shipping\Observer;

use Magento\Framework\Event\ObserverInterface;

class ConvertQuoteToOrder implements ObserverInterface {

    /**
     * Set delivery branch id and name
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $quote = $observer->getQuote();
        $order = $observer->getOrder();
        $order->setDeliveryBranchId($quote->getDeliveryBranchId());
        $order->setDeliveryBranchName($quote->getDeliveryBranchName());
    }

}
