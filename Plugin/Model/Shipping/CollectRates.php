<?php

namespace Shirtplatform\Shipping\Plugin\Model\Shipping;
use Magento\Store\Model\ScopeInterface;

/**
 * Class CollectRates
 * @package Shirtplatform\Shipping\Plugin\Model\Shipping
 */
class CollectRates extends \Magento\Framework\DataObject
{

    /** @var string */
    const MAPPING_PATH = 'carriers/methods_order/mapping';

    /** @var \Magento\Framework\App\Config\ScopeConfigInterface */
    private $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    )
    {
        parent::__construct($data);
        $this->scopeConfig = $scopeConfig;
    }

    public function afterCollectRates(\Magento\Shipping\Model\Shipping $subject, \Magento\Shipping\Model\Shipping $result)
    {
        $rates = $subject->getResult()->getAllRates();
        $subject->getResult()->reset();

        $this->sortRates($rates);

        foreach($rates as $rate)
        {
            $subject->getResult()->append($rate);
        }
        return $result;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address\RateResult\Method[] $rates
     */
    private function sortRates(array &$rates)
    {
        $mapping = $this->getMapping();
        $sorted = [];
        $unsorted = [];
        foreach($rates as $rate)
        {
            $suffix = $rate->getCarrier() . '_' . $rate->getMethod();
            if(isset($mapping[$suffix]))
            {
                $sorted[$mapping[$suffix]] = $rate;
            }
            else
            {
                $unsorted[$rate->getPrice() . '_' . $rate->getMethod()] = $rate;
            }
        }
        ksort($sorted);
        ksort($unsorted);

        $rates = [];
        foreach($sorted as $rate)
        {
            $rates[] = $rate;
        }

        foreach($unsorted as $rate)
        {
            $rates[] = $rate;
        }
    }

    /**
     * @return array
     */
    public function getMapping()
    {
        $string = $this->scopeConfig->getValue(self::MAPPING_PATH, ScopeInterface::SCOPE_STORE);
        $data =  ($string == null) ? [] : json_decode($string, true);
        $result = [];
        foreach($data as $row)
        {
            $result[$row['method']] = (int) $row['position'];
        }
        return $result;
    }

}
