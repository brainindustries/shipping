<?php

namespace Shirtplatform\Shipping\Plugin\Model\Shipping;

class Info extends \Magento\Framework\DataObject {

    /**
     * Track IDs to be stored in core registry
     *
     * @var array
     */
    private $_trackIds = [];

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    private $_coreRegistry;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $_orderRepository;

    /**
     * Shipping data
     *
     * @var \Magento\Shipping\Helper\Data
     */
    private $_shippingHelper;

    /**
     * @var \Magento\Sales\Api\ShipmentRepositoryInterface
     */
    private $_shipmentRepository;

    /**
     *
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Shipping\Helper\Data $shippingHelper
     * @param \Magento\Sales\Api\ShipmentRepositoryInterface $shipmentRepository
     */
    public function __construct(\Magento\Framework\Registry $coreRegistry,
                                \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
                                \Magento\Shipping\Helper\Data $shippingHelper,
                                \Magento\Sales\Api\ShipmentRepositoryInterface $shipmentRepository
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_orderRepository = $orderRepository;
        $this->_shippingHelper = $shippingHelper;
        $this->_shipmentRepository = $shipmentRepository;
    }

    /**
     * Saves available track IDs to core registry before calling loadByHash method.
     * This is used in platform carrier models when getting tracking information as
     * a check method if the tracking number belongs to one of these tracks
     *
     * @access public
     * @param \Magento\Shipping\Model\Info $subject
     * @param string $hash
     * @return string
     */
    public function beforeLoadByHash($subject,
                                     $hash) {
        $data = $this->_shippingHelper->decodeTrackingHash($hash);
        if (!empty($data)) {
            $this->setData($data['key'], $data['id']);

            if ($this->getOrderId()) {
                $order = $this->_orderRepository->get($this->getOrderId());

                foreach ($order->getShipmentsCollection() as $shipment) {
                    foreach ($shipment->getAllTracks() as $track) {
                        $this->_trackIds[] = $track->getId();
                    }
                }
            }
            elseif ($this->getShipId()) {
                $shipment = $this->_shipmentRepository->get($this->getShipId());

                foreach ($shipment->getAllTracks() as $track) {
                    $this->_trackIds[] = $track->getId();
                }
            }
            else {
                $this->_trackIds[] = $this->getTrackId();
            }

            $this->_coreRegistry->register('shipment_popup_track_ids', $this->_trackIds, true);
        }

        return [$hash];
    }

}
