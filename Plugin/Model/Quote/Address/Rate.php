<?php

namespace Shirtplatform\Shipping\Plugin\Model\Quote\Address;

class Rate {
    /**
     * Add used_in_additional_logic flag to address rate
     * 
     * @access public
     * @param \Magento\Quote\Model\Quote\Address\Rate $subject
     * @param \Closure $proceed
     * @param \Magento\Quote\Model\Quote\Address\RateResult\AbstractResult $rate
     * @return type
     */
    public function aroundImportShippingRate($subject, $proceed, $rate) {
        $result = $proceed($rate);        
        
        if ($rate instanceof \Magento\Quote\Model\Quote\Address\RateResult\Method) {
            $result->setUsedInAdditionalLogic($rate->getUsedInAdditionalLogic());       
            $result->setDescription($rate->getDescription());
        }
        
        return $result;
    }
}