<?php

namespace Shirtplatform\Shipping\Plugin\Model\Quote;

/**
 * Class Address
 * @package Shirtplatform\Shipping\Plugin\Model\Quote
 */
class Address {

    /**
     * @var \Magento\Shipping\Model\CarrierFactoryInterface
     */
    private $_carrierFactory;

    /**
     * 
     * @param \Magento\Shipping\Model\CarrierFactoryInterface $carrierFactory
     */
    public function __construct(\Magento\Shipping\Model\CarrierFactoryInterface $carrierFactory) {
        $this->_carrierFactory = $carrierFactory;
    }

    /**
     * @deprecated This is probably not needed. It was done a very long time ago and doesn't implement sorting
     * @param \Magento\Quote\Model\Quote\Address $subject
     * @param callable $proceed     
     * @return array
     */
    public function aroundGetGroupedAllShippingRates(\Magento\Quote\Model\Quote\Address $subject,
                                                     callable $proceed) {

        return $proceed();
        // $rates = [];
        // foreach ($subject->getShippingRatesCollection() as $rate) {
        //     if (!$rate->isDeleted() && $this->_carrierFactory->get($rate->getCarrier())) {
        //         $rates[0][] = $rate;
        //     }
        // }
        // return $rates;
    }

}
