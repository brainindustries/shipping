<?php

namespace Shirtplatform\Shipping\Plugin\Model\Quote\Cart;

use Magento\Store\Model\ScopeInterface;

class ShippingMethodConverter {

    /**
     * @var \Magento\Quote\Api\Data\ShippingMethodExtensionFactory
     */
    private $_extensionFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * @var \Magento\Tax\Model\Calculation
     */
    private $_taxCalculation;

    /**
     * @var \Magento\Tax\Model\Config
     */
    private $_taxConfig;

    /**
     * 
     * @param \Magento\Quote\Api\Data\ShippingMethodExtensionFactory $extensionFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Tax\Model\Calculation $taxCalculation
     * @param \Magento\Tax\Model\Config $taxConfig
     */
    public function __construct(\Magento\Quote\Api\Data\ShippingMethodExtensionFactory $extensionFactory,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\Tax\Model\Calculation $taxCalculation,
                                \Magento\Tax\Model\Config $taxConfig) {
        $this->_extensionFactory = $extensionFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_taxCalculation = $taxCalculation;
        $this->_taxConfig = $taxConfig;
    }    

    /**
     * Set used_in_additional_logic for shipping rate and calculate correct
     * prices including and excluding tax
     * 
     * @access public
     * @param \Magento\Quote\Model\Cart\ShippingMethodConverter $subject
     * @param \Magento\Quote\Api\Data\ShippingMethodInterface $result
     * @param \Magento\Quote\Model\Quote\Address\Rate $rateModel
     * @param string $quoteCurrencyCode
     * @return \Magento\Quote\Api\Data\ShippingMethodInterface
     */
    public function afterModelToDataObject($subject,
                                           $result,
                                           $rateModel,
                                           $quoteCurrencyCode) {
        $extensionAttributes = $result->getExtensionAttributes();
        if ($extensionAttributes == null) {
            $extensionAttributes = $this->_extensionFactory->create();
        }

        $extensionAttributes->setUsedInAdditionalLogic($rateModel->getUsedInAdditionalLogic());
        $extensionAttributes->setDescription($rateModel->getDescription());
        $result->setExtensionAttributes($extensionAttributes);

        $storeId = $rateModel->getAddress()->getQuote()->getStoreId();
        $priceExclTax = $result->getPriceExclTax();
        $priceInclTax = $result->getPriceInclTax();
        
        //set correct shipping prices - bug in magento core
        if ($priceExclTax == $priceInclTax) {            
            $taxRateId = $this->_scopeConfig->getValue('tax/classes/shipping_tax_class', ScopeInterface::SCOPE_STORE, $storeId);
            $request = $this->_taxCalculation->getRateRequest(null, null, null, $storeId);
            $request->setProductClassId($taxRateId);
            $taxPercent = $this->_taxCalculation->getRate($request);

            if ($this->_taxConfig->shippingPriceIncludesTax($storeId)) {
                $newPriceExclTax = round($priceInclTax / (1 + $taxPercent / 100), 2);
                $result->setPriceExclTax($newPriceExclTax);
            }
            else {
                $newPriceInclTax = round($priceExclTax + $priceExclTax * ($taxPercent / 100), 2);
                $result->setPriceInclTax($newPriceInclTax);
            }
        }

        $roundPriceInclTax = $this->_scopeConfig->getValue('tax/calculation/round_shipping_incl_tax', ScopeInterface::SCOPE_STORE, $storeId);
        if ($roundPriceInclTax) {
            $lowerPriceInclTax = floor($priceInclTax);
            $higherPriceInclTax = ceil($priceInclTax);

            $priceInclTax = $result->getPriceInclTax();
            if (abs($priceInclTax - $lowerPriceInclTax) > 0.001 && abs($priceInclTax - $lowerPriceInclTax) < 0.02) {
                $result->setPriceInclTax($lowerPriceInclTax);
            }
            elseif (abs($priceInclTax - $higherPriceInclTax) > 0.001 && abs($priceInclTax - $higherPriceInclTax) < 0.02) {
                $result->setPriceInclTax($higherPriceInclTax);
            }
        }

        return $result;
    }

}
