<?php

namespace Shirtplatform\Shipping\Plugin\Model\Quote;

class QuoteManagement {
    
    /**
     * Copy packstation fields
     * 
     * @access public
     * @param \Magento\Quote\Model\QuoteManagement $subject
     * @param \Magento\Quote\Model\Quote $quote
     * @param array $orderData
     * @return array
     */
    public function beforeSubmit($subject, $quote, $orderData = []) {
        if (!$quote->isVirtual()) {
            $shippingAddress = $quote->getShippingAddress();

            if ($shippingAddress->getPostNumber() and $shippingAddress->getPackstationNumber()) {
                $shippingAddress->setStreet('Packstation');
                $shippingAddress->setStreetNumber($shippingAddress->getPackstationNumber());
                $shippingAddress->setCareOf($shippingAddress->getPostNumber());
            }
        }
        
        return [$quote, $orderData];
    }
}