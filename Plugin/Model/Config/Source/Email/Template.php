<?php

namespace Shirtplatform\Shipping\Plugin\Model\Config\Source\Email;

class Template {
    /**
     * Generate list of email templates. Adds our custom shipment email template
     * 
     * @access public
     * @param \Magento\Config\Model\Config\Source\Email\Template $subject
     * @param array $result
     * @return array
     */
    public function afterToOptionArray($subject, $result) {
        if (in_array($subject->getPath(), ['sales_email/shipment/template', 'sales_email/shipment/guest_template'])) {
            $templateId = 'shirtplatform_shipping_default_template';
            $templateLabel = 'Shirtplatform Shipping - Default template';
            array_unshift($result, ['value' => $templateId, 'label' => $templateLabel]);
        }
        
        return $result;
    }
}