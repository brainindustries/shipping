<?php

namespace Shirtplatform\Shipping\Plugin\Helper\Shipping;

class Data
{

    /**
     * Shipping tracking popup URL getter. Get direct tracking URL from $track if it exists.
     *
     * @access public
     * @param \Magento\Sales\Model\AbstractModel $model
     * @return string
     */
    public function aroundGetTrackingPopupUrlBySalesModel($subject, $proceed, $model)
    {
        $shipment = $track = null;

        if ($model instanceof \Magento\Sales\Model\Order) {
            $shipmentsCol = $model->getShipmentsCollection();
            if (count($shipmentsCol) == 1) {
                $shipment = $shipmentsCol->getFirstItem();
            }
        } elseif ($model instanceof \Magento\Sales\Model\Order\Shipment) {
            $shipment = $model;
        } elseif ($model instanceof \Magento\Sales\Model\Order\Shipment\Track) {
            $shipment = $model->getShipment();
            $track = $model;
        }
        
        if ($shipment == null) {
            return $proceed($model);
        }

        if ($track == null) {
            $allTracks = $shipment->getAllTracks();

            //more tracking numbers
            if (count($allTracks) != 1) {
                return $proceed($model);
            }

            $track = reset($allTracks);
        }

        return $track->getTrackingUrl() ?? $proceed($model);
    }
}
