<?php

namespace Shirtplatform\Shipping\Plugin\Block\Customer\Address;

class DefaultRenderer {

    /**
     * Add "Packstation" or "c/o" to customer address if needed
     * 
     * @access public
     * @param \Magento\Customer\Block\Address\Renderer\DefaultRenderer $subject
     * @param array $addressAttributes
     * @param Format|null $format
     * @return array
     */
    public function beforeRenderArray($subject,
                                      $addressAttributes,
                                      $format = null) {
        if (!empty($addressAttributes['care_of'])) {
            $street = is_array($addressAttributes['street']) ? $addressAttributes['street'][0] : $addressAttributes['street'];
            if ($street == 'Packstation') {
                $addressAttributes['care_of'] = __('Post number') . ': ' . $addressAttributes['care_of'];
            }
            else {
                $addressAttributes['care_of'] = 'c/o ' . $addressAttributes['care_of'];
            }
        }

        return [$addressAttributes, $format];
    }

}
