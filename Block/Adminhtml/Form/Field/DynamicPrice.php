<?php

namespace Shirtplatform\Shipping\Block\Adminhtml\Form\Field;

class DynamicPrice extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
    protected function _prepareToRender()
    {
        $this->addColumn('min_cart_price', ['label' => __('Min. Cart Price'), 'class' => 'required-entry validate-number validate-zero-or-greater']);        
        $this->addColumn('price', ['label' => __('Price'), 'class' => 'required-entry validate-number validate-zero-or-greater']);        
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }
}