<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 26.04.2019
 * Time: 12:43
 */

namespace Shirtplatform\Shipping\Block\Adminhtml\System\Config\Form\Field;

/**
 * Class OrderMapping
 * @package Shirtplatform\Shipping\Block\Adminhtml\System\Config\Form\Field
 */
class OrderMapping extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{

    /** @var ShippingMethodRenderer */
    private $shippingMethodRenderer;

    /** @var \Magento\Shipping\Model\Config */
    private $shippingConfig;

    /**
     * OrderMapping constructor.
     * @param \Magento\Shipping\Model\Config $shippingConfig
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Shipping\Model\Config $shippingConfig,
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    )
    {
        $this->shippingConfig = $shippingConfig;
        parent::__construct($context, $data);
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _construct()
    {
        $this->addColumn('method', [
            'label' => __('Shipping Method'),
            'renderer' => $this->getShippingMethodRenderer()
        ]);
        $this->addColumn('position', [
            'label' => __('Position'),
        ]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
        parent::_construct();
    }

    /**
     * @return \Magento\Framework\View\Element\BlockInterface|ShippingMethodRenderer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getShippingMethodRenderer()
    {
        if($this->shippingMethodRenderer === null)
        {
            $this->shippingMethodRenderer = $this->getLayout()->createBlock(
                ShippingMethodRenderer::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
            $this->createShippingMethodOptions();
        }
        return $this->shippingMethodRenderer;
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function createShippingMethodOptions()
    {
        foreach($this->shippingConfig->getAllCarriers() as $carrierCode => $carrierModel)
        {
            foreach($carrierModel->getAllowedMethods() as $methodId => $methodTitle)
            {
                $this->getShippingMethodRenderer()->addOption($carrierCode . '_' . $methodId, '[' . strtoupper($carrierCode) . '] ' . $methodTitle . ' - ' . $methodId);
            }
        }
    }

    /**
     * @param \Magento\Framework\DataObject $row
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareArrayRow(\Magento\Framework\DataObject $row)
    {
        $row->setData(
            'option_extra_attrs',
            [
                'option_' . $this->getShippingMethodRenderer()->calcOptionHash($row->getData('method')) => 'selected="selected"'
            ]
        );
    }

}
