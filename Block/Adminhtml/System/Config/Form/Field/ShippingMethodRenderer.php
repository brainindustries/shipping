<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 26.04.2019
 * Time: 12:50
 */

namespace Shirtplatform\Shipping\Block\Adminhtml\System\Config\Form\Field;

/**
 * Class ShippingMethodRenderer
 * @package Shirtplatform\Shipping\Block\Adminhtml\System\Config\Form\Field
 */
class ShippingMethodRenderer extends \Magento\Framework\View\Element\Html\Select
{

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->getInputName();
    }

}
