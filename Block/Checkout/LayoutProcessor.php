<?php

namespace Shirtplatform\Shipping\Block\Checkout;

class LayoutProcessor implements \Magento\Checkout\Block\Checkout\LayoutProcessorInterface {

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * 
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig) {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * Add Packstation fields to shipping address form
     * 
     * @access public
     * @param array $jsLayout
     * @return array
     */
    public function process($jsLayout) {
        $usePackstation = $this->_scopeConfig->getValue('checkout/options/use_packstation', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        if ($usePackstation) {
            if (isset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'])) {
                $packstationFields['post_number'] = [
                    'component' => 'Magento_Ui/js/form/element/abstract',
                    'config' => [
                        'customScope' => 'shippingAddress',
                        'template' => 'ui/form/field',
                        'elementTmpl' => 'ui/form/element/input'                        
                    ],
                    'dataScope' => 'shippingAddress.post_number',
                    'label' => __('Post number'),
                    'provider' => 'checkoutProvider',
                    'required' => true,
                    'validation' => ['required-entry' => true, 'validate-number' => true],                    
                    'cnf' => [
                        'field_id' => 'post_number',
                        'enabled' => 1,
                        'required' => true
                    ]
                ];

                $packstationFields['packstation_number'] = [
                    'component' => 'Magento_Ui/js/form/element/abstract',
                    'config' => [
                        'customScope' => 'shippingAddress',
                        'template' => 'ui/form/field',
                        'elementTmpl' => 'ui/form/element/input'                        
                    ],
                    'dataScope' => 'shippingAddress.packstation_number',
                    'label' => __('Packstation number'),
                    'provider' => 'checkoutProvider',
                    'required' => true,
                    'validation' => ['required-entry' => true, 'validate-number' => true],
                    'cnf' => [
                        'field_id' => 'packstation_number',
                        'enabled' => 1,
                        'required' => true
                    ]
                ];

                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['post_number'] = $packstationFields['post_number'];
                $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['packstation_number'] = $packstationFields['packstation_number'];
            }
        }

        return $jsLayout;
    }

}
