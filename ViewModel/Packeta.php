<?php

namespace Shirtplatform\Shipping\ViewModel;

use Magento\Framework\Session\SessionManager;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Shirtplatform\Core\Helper\Data as ShirtplatformHelper;

class Packeta implements ArgumentInterface
{
    /**
     * @var ShirtplatformHelper
     */
    private $helper;    

    /**
     * Here we use instance of SessionManager due to the fact that frotnend and adminhtml
     * use different class for session management that both extends from SessionManager.
     * The concrete implementation is set in frontend/di.xml and adminhtml/di.xml
     * 
     * @var SessionManager
     */
    private $sessionManager;

    /**
     * @param ShirtplatformHelper $helper
     * @param SessionManager $sessionManager
     */
    public function __construct(
        ShirtplatformHelper $helper,
        SessionManager $sessionManager
    ) {
        $this->helper = $helper;
        $this->sessionManager = $sessionManager;
    }

    /**
     * Get quote mode object
     * 
     * @return \Magento\Quote\Model\Quote
     */
    private function getQuote()
    {
        return $this->sessionManager->getQuote();
    }

    /**
     * Get delivery branch ID
     * 
     * @access public
     * @return string
     */
    public function getDeliveryBranchId()
    {
        return $this->getQuote()->getDeliveryBranchId();
    }

    /**
     * Get delivery branch name
     * 
     * @access public
     * @return string
     */
    public function getDeliveryBranchName()
    {
        $branchName = $this->getQuote()->getDeliveryBranchName();
        $shippingAddress = $this->getQuote()->getShippingAddress();

        if ($this->getDeliveryBranchId()) {
            $street = join(' ', $shippingAddress->getStreet());
            $branchName .= ' ' . $street . ' '
                . $shippingAddress->getStreetNumber() . ', '
                . $shippingAddress->getCity();
        }

        return $branchName;
    }

    /**
     * Get Zasielkovna API key
     * 
     * @access public
     * @return string
     */
    public function getZasielkovnaKey()
    {
        return $this->helper->getConfigValue(
            'carriers/shirtplatformShipping/zasielkovna_key',
            $this->getQuote()->getStoreId()
        );
    }

    /**
     * Get allowed countries
     * 
     * @access public
     * @return string
     */
    public function getAllowedCountries()
    {
        return $this->helper->getConfigValue(
            'general/country/allow',
            $this->getQuote()->getStoreId()
        );
    }
}
