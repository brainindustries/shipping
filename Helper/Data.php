<?php

namespace Shirtplatform\Shipping\Helper;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Store\Model\ScopeInterface;
use Shirtplatform\Core\Model\Cache\Type\Api as ShirtplatformCacheApi;
use Shirtplatform\Shipping\Model\Carrier\Shipping as ShirtplatformCarrier;
use shirtplatform\entity\localization\ShippingCarrierLocalized;
use shirtplatform\entity\open\ShippingModuleProvider;
use shirtplatform\entity\shipping\ShippingCarrier;
use shirtplatform\entity\shipping\ShippingFreeDeliveryPrice;
use shirtplatform\entity\shipping\ShippingModule;
use shirtplatform\entity\shipping\ShippingZone;
use shirtplatform\entity\shipping\ShippingZoneDestination;
use shirtplatform\entity\shipping\ShippingZonePrice;
use shirtplatform\filter\Filter;
use shirtplatform\filter\WsParameters;
use shirtplatform\parser\WsParse;
use shirtplatform\resource\PublicResource;
use shirtplatform\rest\REST;

/**
 * Class Data
 * @package Shirtplatform\Shipping\Helper
 */
class Data extends AbstractHelper
{
    /**
     * API response data cache lifetime (in seconds)
     */
    const CACHE_LIFETIME = 7200;

    /** @var int */
    const PROVIDER_DHL_STANDARD = 1;

    /** @var int */
    const PROVIDER_DHL_EXPRESS_ONE_DAY = 20;

    /** @var int */
    const PROVIDER_DEUTCHEPOST = 5;

    /** @var int */
    const PROVIDER_SHIPPYPRO_DEPOST = 60;

    /** @var string */
    const PLATFORM_SHIPPING_ATTRIBUTE_VALUE = 'SendByPost';

    /**
     * @var \Magento\Framework\App\CacheInterface
     */
    private $_cache;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    private $_cacheState;

    /**
     * @var \Magento\Framework\Math\Random
     */
    private $_randomDataGenerator;

    /** @var \Shirtplatform\Core\Helper\Data */
    private $shirtplatformDataHelper;

    /** @var StoreManagerInterface */
    private $storeManager;

    /** @var \Magento\Framework\App\State */
    private $state;

    /**
     * @var ShippingFreeDeliveryPrice[]
     */
    private $_freeDeliveryPrices;

    /** @var ShippingModule[] */
    private $shippingModules;

    /** @var ShippingCarrier[] */
    private $shippingCarriers;

    /**
     * @var ShippingZone[]
     */
    private $_shippingZones = [];

    /**
     * Store ID that will be used to authenticate with Shirtplatform API
     * 
     * @var int
     */
    private $_requestStoreId;

    /**
     * Data constructor.
     * @param Context $context     
     * @param \Magento\Framework\App\CacheInterface $cache
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState     
     * @param \Magento\Framework\Math\Random $randomDataGenerator
     * @param \Shirtplatform\Core\Helper\Data $shirtplatformDataHelper
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\State $state     
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\CacheInterface $cache,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\Math\Random $randomDataGenerator,
        \Shirtplatform\Core\Helper\Data $shirtplatformDataHelper,
        StoreManagerInterface $storeManager,
        \Magento\Framework\App\State $state
    ) {
        parent::__construct($context);
        $this->_cache = $cache;
        $this->_cacheState = $cacheState;
        $this->_randomDataGenerator = $randomDataGenerator;
        $this->shirtplatformDataHelper = $shirtplatformDataHelper;
        $this->storeManager = $storeManager;
        $this->state = $state;
    }

    /**
     * Get country for this platform store
     *
     * @access private
     * @param int $storeId
     * @return int
     */
    private function _getCountry($storeId)
    {
        return $this->scopeConfig->getValue('shirtplatform/general/country', ScopeInterface::SCOPE_STORE, $storeId);
    }

    /**
     * Get language for this platform store
     *
     * @access private
     * @param int $storeId
     * @return int
     */
    private function _getLanguage($storeId)
    {
        return $this->scopeConfig->getValue('shirtplatform/general/language', ScopeInterface::SCOPE_STORE, $storeId);
    }

    /**
     * Is Shirtplatform API cache enabled
     * 
     * @access private
     * @return boolean
     */
    private function _isCacheEnabled()
    {
        return $this->_cacheState->isEnabled(ShirtplatformCacheApi::TYPE_IDENTIFIER);
    }

    /**
     * Get hash for cache key
     *
     * @access private
     * @param string $cacheKey
     * @return string
     */
    private function _getHashForCacheKey($cacheKey)
    {
        $hashesCacheKey = ShirtplatformCacheApi::CACHE_TAG . '_hashes';
        $existingHashes = $this->_cache->load($hashesCacheKey);
        $existingHashes = $existingHashes ? unserialize($existingHashes) : [];

        if (isset($existingHashes[$cacheKey])) {
            return $existingHashes[$cacheKey];
        }

        do {
            $newHash = $this->_randomDataGenerator->getRandomString(16);
            $hashExists = false;

            foreach ($existingHashes as $hash) {
                if ($newHash == $hash) {
                    $hashExists = true;
                    break;
                }
            }
        } while ($hashExists);

        $existingHashes[$cacheKey] = $newHash;
        $this->_cache->save(
            serialize($existingHashes),
            $hashesCacheKey,
            [ShirtplatformCacheApi::CACHE_TAG],
            self::CACHE_LIFETIME
        );
        return $newHash;
    }

    /**
     * @return int|null
     */
    private function getDefaultStoreViewId()
    {
        foreach ($this->storeManager->getStores(true) as $store) {
            if ($store->getCode() === 'admin') {
                continue;
            }
            return $store->getId();
        }
        return null;
    }

    /**
     * Get DHL tracking link
     * 
     * @access public
     * @param \Magento\Sales\Model\Order\Shipment\Track $track
     * @deprecated There's the field tracking_url in sales_shipment_track table. Use this.
     * @return string
     */
    public function getDhlTrackingLink($track)
    {
        return 'http://nolp.dhl.de/nextt-online-public/set_identcodes.do?lang=de&idc=' . $track->getTrackNumber() . '&rfn=&extendedSearch=true';
    }

    /**
     * @param $providerId
     * @param int|null $storeId
     * @return null|ShippingModuleProvider
     * @throws \Exception
     */
    public function getModuleProviderById(
        $providerId,
        $storeId = null
    ) {
        $auth = $this->shirtplatformDataHelper->shirtplatformAuth(($storeId != null) ? $storeId : $this->getDefaultStoreViewId());
        if (!$auth) {
            return null;
        }

        return ShippingModuleProvider::find($providerId);
    }

    /**
     * Authenticate to shirtplatform using currently set store ID
     * 
     * @access private
     * @return bool
     */
    private function _shirtplatformAuth()
    {
        $storeId = $this->_requestStoreId ?: $this->getDefaultStoreViewId();
        return $this->shirtplatformDataHelper->shirtplatformAuth($storeId);
    }

    /**
     * Wrapper method that gets API data. First it tries to load it from cache and if 
     * not present, it calls the method defined in $callable and save the result to cache
     *
     * @access private
     * @param string $cacheKey
     * @param array $callable 1st item - method, 2nd item - arguments
     * @return mixed
     */
    private function _getApiData($cacheKey, $callable)
    {
        if (!(is_array($callable) && count($callable) == 2)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('%1 expects second parameter to be array with 2 items', '\Shirtplatform\Shipping\Helper\Data::_getApiData()')
            );
        }

        if ($this->_isCacheEnabled()) {
            $data = $this->_cache->load($cacheKey);
            if ($data) {
                $data = unserialize($data);
            } else {
                if (!$this->_shirtplatformAuth()) {
                    return [];
                }

                $data = call_user_func_array($callable[0], $callable[1]);
                $this->_cache->save(
                    serialize($data),
                    $cacheKey,
                    [ShirtplatformCacheApi::CACHE_TAG],
                    self::CACHE_LIFETIME
                );
            }
        } else {
            if (!$this->_shirtplatformAuth()) {
                return [];
            }

            $data = call_user_func_array($callable[0], $callable[1]);
        }

        return $data;
    }

    /**
     * @return array|ShippingModuleProvider[]
     * @throws \Exception
     */
    public function getShippingModuleProviders()
    {
        $auth = $this->shirtplatformDataHelper->shirtplatformAuth($this->getDefaultStoreViewId());
        if (!$auth) {
            return [];
        }
        return ShippingModuleProvider::findAll();
    }

    /**
     * @return ShippingModule[]
     */
    private function getShippingModules()
    {
        if ($this->shippingModules === null) {
            $cacheKey = ShirtplatformCacheApi::TYPE_IDENTIFIER . '_shipping_modules';
            $callable = [[$this, '_callShippingModuleApi'], []];
            $this->shippingModules = $this->_getApiData($cacheKey, $callable);
        }
        return $this->shippingModules;
    }

    /**
     * Call ShippingModule API and remove apiUser and apiKey from the result
     *
     * @access private
     * @return void
     */
    private function _callShippingModuleApi()
    {
        $shippingModules = ShippingModule::findAll();
        if (is_array($shippingModules)) {
            foreach ($shippingModules as $moduleId => $module) {
                $shippingModules[$moduleId]->apiUser = '';
                $shippingModules[$moduleId]->apiKey = '';
            }
        }
        return $shippingModules;
    }

    /**
     * Get shipping carriers for shipping modules
     * 
     * @access private
     * @return ShippingCarrier[]
     */
    private function getShippingCarriers()
    {
        if ($this->shippingCarriers === null) {
            $this->shippingCarriers = [];
            $cacheKey = ShirtplatformCacheApi::TYPE_IDENTIFIER . '_shipping_carriers';
            foreach ($this->getShippingModules() as $shippingModule) {
                $cacheKey .= '_' . $shippingModule->id;
            }

            $hash = $this->_getHashForCacheKey($cacheKey);
            $cacheKey = ShirtplatformCacheApi::TYPE_IDENTIFIER . '_shipping_carriers_' . $hash;
            $callable = [[$this, '_callShippingCarrierApi'], []];
            $this->shippingCarriers = $this->_getApiData($cacheKey, $callable);

            foreach ($this->shippingCarriers as $moduleId => &$moduleCarriers) {
                foreach ($moduleCarriers as $carrierId => $carrier) {
                    if ($carrier->moduleProvider == NULL) {
                        unset($moduleCarriers[$carrierId]);
                    }
                }
            }
        }
        return $this->shippingCarriers;
    }

    /**
     * Call ShippingCarrier::findAll API for all shipping modules
     *
     * @access private
     * @return ShippingCarrier[]
     */
    private function _callShippingCarrierApi()
    {
        $wsParams = new WsParameters();
        $wsParams->addSimpleExpression('active', '=', true);
        foreach ($this->getShippingModules() as $shippingModule) {
            $shippingCarriers[$shippingModule->id] = &ShippingCarrier::findAll($shippingModule->id, $wsParams, null, true);
        }
        REST::getInstance()->wait();
        return $shippingCarriers;
    }

    /**
     * Get shipping this->_shippingZones for carriers
     *
     * @access private
     * @param  ShippingCarrier[] $carriers
     * @return ShippingZone[]
     */
    private function _getShippingZonesForCarriers($carriers)
    {
        $cacheKey = ShirtplatformCacheApi::CACHE_TAG . '_shipping_zones';
        foreach ($carriers as $moduleId => $moduleCarriers) {
            foreach ($moduleCarriers as $_carrier) {
                $cacheKey .= '_' . $_carrier->id;
            }
        }

        $hash = $this->_getHashForCacheKey($cacheKey);
        $cacheKey = ShirtplatformCacheApi::CACHE_TAG . '_shipping_zones_' . $hash;
        $callable = [[$this, '_callShippingZoneApi'], [$carriers]];
        $this->_shippingZones = $this->_getApiData($cacheKey, $callable);
        return $this->_shippingZones;
    }

    /**
     * Call ShippingZone API
     *
     * @access private
     * @param  ShippingCarrier[] $carriers
     * @return ShippingZone[]
     */
    private function _callShippingZoneApi($carriers)
    {
        $shippingZones = [];
        foreach ($carriers as $moduleId => $moduleCarriers) {
            foreach ($moduleCarriers as $_carrier) {
                $shippingZones[$moduleId][$_carrier->id] = &ShippingZone::findAll($_carrier->getParentList(), null, null, true);
            }
        }
        REST::getInstance()->wait();
        return $shippingZones;
    }

    /**
     * Get free delivery prices for carriers
     *
     * @access private
     * @param  ShippingCarrier[] $carriers
     * @return ShippingFreeDeliveryPrice[]
     */
    private function _getFreeDeliveryPricesForCarriers($carriers)
    {
        $cacheKey = ShirtplatformCacheApi::CACHE_TAG . '_shipping_free_delivery_price';
        $this->_freeDeliveryPrices = [];
        $isFreeDelivery = false;

        foreach ($carriers as $moduleId => $moduleCarriers) {
            foreach ($moduleCarriers as $_carrier) {
                if ($_carrier->freeDelivery) {
                    $cacheKey .= '_' . $_carrier->id;
                    $isFreeDelivery = true;
                }
            }
        }

        if ($isFreeDelivery) {
            $hash = $this->_getHashForCacheKey($cacheKey);
            $cacheKey = ShirtplatformCacheApi::CACHE_TAG . '_shipping_free_delivery_price_' . $hash;
            $callable = [[$this, '_callFreeDeliveryPriceApi'], [$carriers]];
            $this->_freeDeliveryPrices = $this->_getApiData($cacheKey, $callable);
        }

        return $this->_freeDeliveryPrices;
    }

    /**
     * Call ShippingFreeDeliveryPrice API
     *
     * @access private
     * @param  ShippingCarrier[] $carriers
     * @return ShippingFreeDeliveryPrice[]
     */
    private function _callFreeDeliveryPriceApi($carriers)
    {
        $freeDeliveryPrices = [];
        foreach ($carriers as $moduleCarriers) {
            foreach ($moduleCarriers as $_carrier) {
                $freeDeliveryPrices[$_carrier->id] = &ShippingFreeDeliveryPrice::findAll($_carrier->getParentList(), null, null, true);
            }
        }
        REST::getInstance()->wait();
        return $freeDeliveryPrices;
    }


    /**
     * Get shipping zone destinations
     *
     * @access private
     * @return ShippingZoneDestination[]
     */
    private function _getShippingZoneDestinations()
    {
        $cacheKey = ShirtplatformCacheApi::CACHE_TAG . '_shipping_zone_destination';
        foreach ($this->_shippingZones as $carrierIndex) {
            foreach ($carrierIndex as $shippingZones) {
                foreach ($shippingZones as $zone) {
                    $cacheKey .= '_' . $zone->id;
                }
            }
        }

        $hash = $this->_getHashForCacheKey($cacheKey);
        $cacheKey = ShirtplatformCacheApi::CACHE_TAG . '_shipping_zone_destination_' . $hash;
        $callable = [[$this, '_callShippingZoneDestinationApi'], []];
        return $this->_getApiData($cacheKey, $callable);
    }

    /**
     * Call ShippingZoneDestination API
     * 
     * @access private
     * @return ShippingZoneDestination[]
     */
    private function _callShippingZoneDestinationApi()
    {
        $destinations = [];
        foreach ($this->_shippingZones as $moduleId => $carrierIndex) {
            foreach ($carrierIndex as $carrierId => $shippingZones) {
                foreach ($shippingZones as $zone) {
                    $destinations[$moduleId][$carrierId][$zone->id] = &ShippingZoneDestination::findAll($zone->getParentList(), null, null, true);
                }
            }
        }

        REST::getInstance()->wait();
        return $destinations;
    }

    /**
     * Get Shipping zone prices
     *
     * @access private
     * @param array $allowedZoneIds
     * @param int $storeId
     * @return ShippingZonePrice[]
     */
    private function _getShippingZonePrices($allowedZoneIds, $storeId)
    {
        $cacheKey = ShirtplatformCacheApi::CACHE_TAG . '_shipping_zone_price_store_' . $storeId;
        foreach ($allowedZoneIds as $moduleId => $carrierIndex) {
            foreach ($carrierIndex as $carrierId => $zoneId) {
                $cacheKey .= '_' . $zoneId;
            }
        }

        $hash = $this->_getHashForCacheKey($cacheKey);
        $countryId = $this->_getCountry($storeId);
        $cacheKey = ShirtplatformCacheApi::CACHE_TAG . '_shipping_zone_price_store_' . $storeId . '_' . $hash;
        $callable = [[$this, '_callShippingZonePriceApi'], [$allowedZoneIds, $countryId]];
        $zonePrices = $this->_getApiData($cacheKey, $callable);
        return $zonePrices;
    }

    /**
     * Call ShippingZonePrice API
     *
     * @access private
     * @param array $allowedZoneIds
     * @param int $countryId shirtplatform store country id
     * @return ShippingZonePrice[]
     */
    private function _callShippingZonePriceApi($allowedZoneIds, $countryId)
    {
        $priceWsParams = new WsParameters();
        $countryFilter = new Filter('\shirtplatform\entity\account\Country');
        $countryFilter->addSimpleExpression('id', '=', $countryId);
        $priceWsParams->addRelatedFilter($countryFilter);

        $zonePrices = [];
        foreach ($allowedZoneIds as $moduleId => $carrierIndex) {
            foreach ($carrierIndex as $carrierId => $zoneId) {
                $zonePrices[$carrierId] = &ShippingZonePrice::findAll([$moduleId, $carrierId, $zoneId], $priceWsParams, null, true);
            }
        }

        REST::getInstance()->wait();
        return $zonePrices;
    }

    /**
     * @param int $storeId
     * @return array
     */
    public function getCarrierLocalizations($storeId)
    {
        $relatedLanguageId = $this->_getLanguage($storeId);
        $localizations = $carrierIndex = [];
        $cacheKey = ShirtplatformCacheApi::CACHE_TAG . '_shipping_carrier_localized_store_' . $storeId;
        foreach ($this->getShippingCarriers() as $moduleId => $carriers) {
            foreach ($carriers as $carrier) {
                $carrierIndex[$carrier->id] = $carrier;
                $cacheKey .= '_' . $carrier->id;
            }
        }

        $hash = $this->_getHashForCacheKey($cacheKey);
        $cacheKey = ShirtplatformCacheApi::CACHE_TAG . '_shipping_carrier_localized_store_' . $storeId . '_' . $hash;
        $callable = [[$this, '_callShippingCarrierLocalizedApi'], [$relatedLanguageId]];
        $localizations = $this->_getApiData($cacheKey, $callable);

        foreach ($localizations as $carrierId => $carrierLocalizations) {
            $carrierLocalizations = WsParse::indexEntityListByEntity($carrierLocalizations, 'language');
            if (isset($carrierLocalizations[$relatedLanguageId])) {
                $localizations[$carrierId] = [
                    'name' => $carrierLocalizations[$relatedLanguageId]->name,
                    'description' => $carrierLocalizations[$relatedLanguageId]->description
                ];
            } else {
                $localizations[$carrierId] = [
                    'name' => $carrierIndex[$carrierId]->name,
                    'description' => $carrierIndex[$carrierId]->description
                ];
            }
        }
        return $localizations;
    }

    /**
     * Call ShippingCarrierLocalized API
     *
     * @access private
     * @param int $languageId shirtplatform shop language id
     * @return ShippingCarrierLocalized[]
     */
    private function _callShippingCarrierLocalizedApi($languageId)
    {
        $wsParams = new WsParameters();
        $languageFilter = new Filter('\shirtplatform\entity\account\Language');
        $languageFilter->addSimpleExpression('id', '=', $languageId);
        $wsParams->addRelatedFilter($languageFilter);

        foreach ($this->getShippingCarriers() as $moduleId => $carriers) {
            foreach ($carriers as $_carrier) {
                $localizations[$_carrier->id] = &ShippingCarrierLocalized::findAll([$moduleId, $_carrier->id], $wsParams, null, true);
            }
        }

        REST::getInstance()->wait();
        return $localizations;
    }

    public function getProviderLocalizationsMapping($storeId)
    {
        $this->shirtplatformDataHelper->shirtplatformAuth($storeId);
        $providerIndex = [];
        foreach ($this->getShippingCarriers() as $moduleId => $carriers) {
            foreach ($carriers as $carrier) {
                $providerIndex[$carrier->id] = $carrier->moduleProvider->id;
            }
        }

        $result = [];
        foreach ($this->getCarrierLocalizations($storeId) as $carrierId => $localization) {
            $result[$providerIndex[$carrierId]] = $localization;
        }
        return $result;
    }

    /**
     * @param int $storeId
     * @return array
     * @throws \Exception
     */
    public function getProviderLogoMapping($storeId)
    {
        $this->shirtplatformDataHelper->shirtplatformAuth($storeId);
        $result = [];
        foreach ($this->getShippingCarriers() as $moduleId => $carriers) {
            foreach ($carriers as $carrier) {
                $parents = $carrier->getParents();
                $result[$carrier->moduleProvider->id] = PublicResource::getShippingCarrierLogoUrl(\shirtplatform\utils\user\User::getAccountId(), $parents[0], $carrier->id, 100, 100);
            }
        }
        return $result;
    }

    /**
     * @param RateRequest $request
     * @return mixed
     * @throws \Exception
     */
    public function getPlatformShippingMethods(RateRequest $request)
    {
        $origRequestStoreId = $this->_requestStoreId;
        $this->_requestStoreId = $request->getStoreId();
        $relatedCountryId = $this->_getCountry($request->getStoreId());     
        $modules = $this->getShippingModules();
        $carriers = $this->getShippingCarriers();

        if (empty($modules) || empty($carriers)) {
            $this->_requestStoreId = $origRequestStoreId;
            return [];
        }

        foreach ($carriers as $moduleId => $moduleCarriers) {
            $carriers[$moduleId] = WsParse::indexEntityListBy($carriers[$moduleId]);
        }

        $allCarriers = [];
        foreach ($carriers as $moduleId => $moduleCarriers) {
            foreach ($moduleCarriers as $carrier) {
                $allCarriers[$carrier->id] = $carrier;
            }
        }

        $this->_shippingZones = $this->_freeDeliveryPrices = [];
        $this->_getShippingZonesForCarriers($carriers);
        $this->_getFreeDeliveryPricesForCarriers($carriers);

        foreach ($this->_freeDeliveryPrices as $carrierId => $prices) {
            foreach ($prices as $k => $price) {
                if ($price->country->id !== $relatedCountryId) {
                    unset($this->_freeDeliveryPrices[$carrierId][$k]);
                }
            }
        }

        $destinations = $this->_getShippingZoneDestinations();
        $allowedZoneIds = $allowedCarriers = [];
        foreach ($destinations as $moduleId => $carrierIndex) {
            foreach ($carrierIndex as $carrierId => $zoneIndex) {
                foreach ($zoneIndex as $zoneId => $destinations) {
                    foreach ($destinations as $destination) {
                        if ($destination->country->code === $request->getDestCountryId()) {
                            $allowedZoneIds[$moduleId][$carrierId] = $zoneId;
                            $allowedCarriers[$moduleId][$carrierId] = $carriers[$moduleId][$carrierId];
                        }
                    }
                }
            }
        }

        $zonePrices = $this->_getShippingZonePrices($allowedZoneIds, $request->getStoreId());
        $carrierPrices = [];
        $orderWeight = ($request->getPackageWeight() != null) ? $request->getPackageWeight() : 0;
        $orderTotal = $request->getBaseSubtotalInclTax();
        foreach ($zonePrices as $carrierId => $prices) {
            $carrier = $allCarriers[$carrierId];
            if ($carrier->freeDelivery) {
                if (!empty($this->_freeDeliveryPrices[$carrierId])) {
                    $freeDeliveryPrice = reset($this->_freeDeliveryPrices[$carrierId]);
                    if ($orderTotal >= $freeDeliveryPrice->price) {
                        $carrierPrices[$carrierId] = 0;
                        continue;
                    }
                }
            }

            $steps = [];
            foreach ($prices as $price) {
                $steps[(int) $price->weightLimit] = $price->price;
            }
            ksort($steps);

            if (empty($steps)) {
                continue;
            }

            if (count($steps) === 1) {
                $carrierPrices[$carrierId] = $steps[-1];
                continue;
            } else {
                //TODO CHECK IF ISSET
                $allAbove = $steps[-1];
                unset($steps[-1]);
            }

            $stepIndex = [];
            foreach ($steps as $k => $v) {
                $stepIndex[] = $k;
            }

            $i = 0;
            foreach ($steps as $stepWeight => $price) {
                if ($stepWeight >= $orderWeight) {
                    break;
                }
                $i++;
            }
            $carrierPrices[$carrierId] = (isset($stepIndex[$i])) ? $steps[$stepIndex[$i]] : $allAbove;
        }

        $this->_requestStoreId = $origRequestStoreId;
        return $this->toShippingResultData($request, $allowedCarriers, $modules, $carrierPrices);
    }

    /**
     * @param RateRequest $request
     * @param array $carriers
     * @param array $modules
     * @param array|null $prices
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function toShippingResultData(
        RateRequest $request,
        array $carriers,
        $modules,
        array $prices = null
    ) {
        $areaCode = $this->state->getAreaCode();
        $additionalLogicEnabled = $this->isAdditionalLogicEnabled($request->getStoreId());
        $carrierInAdditionalLogic = null;

        if ($areaCode != 'adminhtml' && $additionalLogicEnabled) {
            $quoteItems = $request->getAllItems();
            $destCountryId = $request->getDestCountryId();
            $carrierInAdditionalLogic = $this->getCarrierUsedInAdditionalLogic($quoteItems, $destCountryId);
        }

        $carrierLocalizations = $this->getCarrierLocalizations($request->getStoreId());

        $result = [];
        foreach ($carriers as $moduleId => $moduleCarriers) {
            foreach ($moduleCarriers as $carrierId => $carrier) {
                if ($prices !== null && !isset($prices[$carrier->id])) {
                    continue;
                }

                $usedInAdditionalLogic = 0;
                if ($carrier->moduleProvider->id == $carrierInAdditionalLogic) {
                    $usedInAdditionalLogic = 1;
                }

                $methodTitle = $carrier->name;
                if (!empty($carrierLocalizations[$carrierId]['name'])) {
                    $methodTitle = $carrierLocalizations[$carrierId]['name'];
                }

                $result[] = [
                    'uniqId' => $carrier->moduleProvider->id,
                    'carrierTitle' => $modules[$moduleId]->name,
                    'methodTitle' => $methodTitle,
                    'price' => ($prices !== null) ? $prices[$carrier->id] : 0,
                    'usedInAdditionalLogic' => $usedInAdditionalLogic
                ];
            }
        }

        return $result;
    }

    /**
     * Get data for shipping method that should be used in additional logic
     * that will be used for composing \Magento\Quote\Model\Quote\Address\RateResult\Method
     * 
     * @access public
     * @param \Magento\Quote\Api\Data\CartInterface $quote
     * @param \Magento\Quote\Model\Cart\ShippingMethod[] $carriers
     * @return array|null
     */
    public function getAdditionalLogicData($quote, $carriers)
    {
        $quoteItems = $quote->getAllItems();
        $destCountryId = $quote->getShippingAddress()->getCountryId();
        $targetCarrierId = $this->getCarrierUsedInAdditionalLogic($quoteItems, $destCountryId);

        foreach ($carriers as $_carrier) {
            if (
                $_carrier->getCarrierCode() == ShirtplatformCarrier::CARRIER_CODE &&
                $_carrier->getMethodCode() == $targetCarrierId
            ) {
                return [
                    'uniqId' => $targetCarrierId,
                    'carrierTitle' => $this->getAdditionalLogicTitle($targetCarrierId, $quote->getStoreId()),
                    'methodTitle' => $this->getAdditionalLogicName($targetCarrierId, $quote->getStoreId()),
                    'price' => $this->getAdditionalLogicPrice($quote->getStoreId()),
                    'usedInAdditionalLogic' => 1
                ];
            }
        }

        return null;
    }

    /**
     * Get ID of carrier that should be used in additional logic
     * 
     * @access private
     * @param \Magento\Quote\Model\Quote\Item[] $quoteItems
     * @param string $destCountryId
     * @return int
     * @throws \Exception
     */
    private function getCarrierUsedInAdditionalLogic($quoteItems, $destCountryId)
    {
        if (
            $this->getPhysicalItemsQty($quoteItems) === 1 && $destCountryId === 'DE' && $this->hasSendByPost($quoteItems)
        ) {
            return self::PROVIDER_DEUTCHEPOST;
        }
        return self::PROVIDER_DHL_STANDARD;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item[] $quoteItems
     * @return bool
     * @throws \Exception
     */
    private function hasSendByPost($quoteItems)
    {
        foreach ($quoteItems as $item) {
            $product = $item->getProduct();
            if ($product->isVirtual() || $item->getParentItem()) {
                continue;
            }

            $hasSendByPost = $this->productHasSendByPost($product);
            if (!$hasSendByPost) {
                return false;
            }
        }

        return true;
    }

    /**
     * Return true if product has sh_shipping attribute equal to 'SendByPost',
     * otherwise return false
     * 
     * @access public
     * @param ProductInterface $product
     * @return bool
     */
    public function productHasSendByPost($product)
    {
        $shShipping = $product->getShShipping();
        if (empty($shShipping)) {
            return false;
        }

        $shShippingAttribute = $product->getResource()->getAttribute('sh_shipping');
        if ($shShippingAttribute && $shShippingAttribute->usesSource()) {
            $shShippingLabel = $shShippingAttribute->getSource()->getOptionText($shShipping);
            return $shShippingLabel == self::PLATFORM_SHIPPING_ATTRIBUTE_VALUE;
        }

        return false;
    }

    /**
     * Get total quantity of physical items in the quote
     * 
     * @access private
     * @param \Magento\Quote\Model\Quote\Item[] $quoteItems
     * @return int
     */
    private function getPhysicalItemsQty($quoteItems)
    {
        $i = 0;
        foreach ($quoteItems as $item) {
            if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                continue;
            }
            $i += $item->getQty();
        }
        return (int) $i;
    }

    /**
     * @param int $storeId
     * @return bool
     */
    public function isAdditionalLogicEnabled($storeId)
    {
        return (bool) $this->scopeConfig->getValue(
            'carriers/shirtplatformAdditionalLogic/active',
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get title of the shipping method used in additional logic
     * 
     * @access private
     * @param int $carrierId
     * @param int $storeId
     * @return string
     */
    private function getAdditionalLogicTitle($carrierId, $storeId)
    {
        $path = 'carriers/shirtplatformAdditionalLogic/dhl_title';
        if ($carrierId == self::PROVIDER_DEUTCHEPOST) {
            $path = 'carriers/shirtplatformAdditionalLogic/depost_title';
        }

        return $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE, $storeId);
    }

    /**
     * Get name of the shipping method used in additional logic
     * 
     * @access private
     * @param int $carrierId
     * @param int $storeId
     * @return string
     */
    private function getAdditionalLogicName($carrierId, $storeId)
    {
        $path = 'carriers/shirtplatformAdditionalLogic/dhl_name';
        if ($carrierId == self::PROVIDER_DEUTCHEPOST) {
            $path = 'carriers/shirtplatformAdditionalLogic/depost_name';
        }

        return $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE, $storeId);
    }

    /**
     * @param int $storeId
     * @return float mixed
     */
    private function getAdditionalLogicPrice($storeId)
    {
        return $this->scopeConfig->getValue(
            'carriers/shirtplatformAdditionalLogic/price',
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @param int $subtotal
     * @param int $qty
     * @return bool
     */
    public function isLargeOrder($subtotal, $qty)
    {
        $storeId = $this->storeManager->getStore()->getId();
        $largeOrderMinPrice = $this->scopeConfig->getValue('sales/general/large_order_min_price', ScopeInterface::SCOPE_STORE, $storeId);
        $largeOrderMinQty = $this->scopeConfig->getValue('sales/general/large_order_min_qty', ScopeInterface::SCOPE_STORE, $storeId);
        return ($largeOrderMinPrice !== null && $subtotal >= $largeOrderMinPrice) || ($largeOrderMinQty !== null && $qty >= $largeOrderMinQty);
    }
}
