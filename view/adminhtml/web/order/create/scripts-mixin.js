define([
    'jquery'
], function ($) {
    'use strict';

    return function (source) {
        AdminOrder.prototype.setShippingMethod = AdminOrder.prototype.setShippingMethod.wrap(function (parentMethod, method) {
            let data = {};
            data['order[shipping_method]'] = method;

            if (method !== window.zasielkovnaShippingMethodName) {
                data['order[shipping_delivery_branch_id]'] = null;
                data['order[shipping_delivery_branch_name]'] = null;

                //set shipping address as billing address if we're switching from Zasielkovna to some other method
                if (window.lastShippingMethod == window.zasielkovnaShippingMethodName) {
                    let shipKey;
                    let billingData = this.serializeData(this.billingAddressContainer);
                    billingData = billingData.toObject();

                    for (let key in billingData) {
                        shipKey = key.replace('billing_address', 'shipping_address');
                        $('[name="' + shipKey + '"]').val(billingData[key]);
                        data[shipKey] = billingData[key];
                    }
                }

                this.loadArea(['shipping_method', 'totals', 'billing_method'], true, data).then(parentMethod(method));
            }
            //Zasielkovna chosen and we have branch data
            else if (window.zasielkovnaData && Object.keys(window.zasielkovnaData).length > 0) {                
                data = this.copyBillingToShipping();                
                data['order[shipping_delivery_branch_id]'] = window.zasielkovnaData['branch_id'];
                data['order[shipping_delivery_branch_name]'] = window.zasielkovnaData['branch_name'];
                data['order[shipping_address][care_of]'] = window.zasielkovnaData['care_of'];
                data['order[shipping_address][street][0]'] = window.zasielkovnaData['street'];
                data['order[shipping_address][street_number]'] = window.zasielkovnaData['street_number'];
                data['order[shipping_address][postcode]'] = window.zasielkovnaData['postcode'];
                data['order[shipping_address][city]'] = window.zasielkovnaData['city'];
                data['order[shipping_address][country_id]'] = window.zasielkovnaData['country_id'];
                data['order[shipping_method]'] = method;                
                this.loadArea(['shipping_method', 'totals', 'billing_method', 'shipping_address'], true, data).then(parentMethod(method));
            }
            //Zasielkovna chosen but we don't have data
            else {
                $('[name="order[shipping_address][care_of]"]').val('');
                $('[name="order[shipping_address][street][0]"]').val('');
                $('[name="order[shipping_address][street_number]"]').val('');
                $('[name="order[shipping_address][postcode]"]').val('');
                $('[name="order[shipping_address][city]"]').val('');
                $('[name="order[shipping_address][country_id]"]').val('');
                this.loadArea(['shipping_method', 'totals', 'billing_method'], true, data).then(parentMethod(method));
            }

            window.lastShippingMethod = method;
        });

        AdminOrder.prototype.setDeliveryBranch = function (point) {
            if (point) {
                let streetParts = point.street.split(' ');
                let streetNum = streetParts[streetParts.length - 1];
                let street = point.street.replace(' ' + streetNum, '');                
                let data = this.copyBillingToShipping();

                //we have to uncheck the checkbox or branch info won't be transferred to form
                if ($('#order-shipping_same_as_billing').is(':checked')) {
                    $('#order-shipping_same_as_billing').click();
                }

                data['order[shipping_delivery_branch_id]'] = window.zasielkovnaData.branch_id = point.id;
                data['order[shipping_delivery_branch_name]'] = window.zasielkovnaData.branch_name = point.place;
                data['order[shipping_address][care_of]'] = window.zasielkovnaData.care_of = point.place;
                data['order[shipping_address][street][0]'] = window.zasielkovnaData.street = street;
                data['order[shipping_address][street_number]'] = window.zasielkovnaData.street_number = streetNum;
                data['order[shipping_address][postcode]'] = window.zasielkovnaData.postcode = point.zip;
                data['order[shipping_address][city]'] = window.zasielkovnaData.city = point.city;
                data['order[shipping_address][country_id]'] = window.zasielkovnaData.country_id = point.country.toUpperCase();

                let branchName = point.place + ', ' + street + ' ' + streetNum + ', ' + point.city;
                document.getElementById('zasielkovna-branch-id').value = point.id;
                document.getElementById('zasielkovna-branch-name').innerText = branchName;

                this.loadArea(['shipping_method', 'totals', 'shipping_address'], true, data).then(function () {
                    if ($('#order-shipping_address_save_in_address_book').is(':checked')) {
                        $('#order-shipping_address_save_in_address_book').prop('checked', false);
                    }
                });
            }
        }

        AdminOrder.prototype.copyBillingToShipping = function () {
            let billingData = this.serializeData(this.billingAddressContainer);
            let shipKey;
            billingData = billingData.toObject();
            let data = this.serializeData(this.shippingAddressContainer);
            data = data.toObject();

            for (let key in billingData) {
                shipKey = key.replace('billing_address', 'shipping_address');
                data[shipKey] = billingData[key];
            }

            return data;
        }

        return source;
    }
});
