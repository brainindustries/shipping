<?php

namespace Shirtplatform\Shipping\Test\Unit\Model\Carrier;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\ResultFactory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Shirtplatform\Shipping\Helper\Data as Helper;
use Shirtplatform\Shipping\Model\Carrier\ShippingStandard;

class ShippingStandardTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $scopeConfigMock;

    /**
     * @var MockObject
     */
    private $errorFactoryMock;

    /**
     * @var MockObject
     */
    private $loggerMock;

    /**
     * @var MockObject
     */
    private $resultFactoryMock;

    /**
     * @var MockObject
     */
    private $methodFactoryMock;

    /**
     * @var MockObject
     */
    private $helperMock;

    /**
     * @var MockObject
     */
    private $rateRequestMock;

    /**
     * @var MockObject
     */
    private $serializerMock;

    /**
     * @var ShippingStandard
     */
    private $model;    

    protected function setUp(): void
    {
        $this->scopeConfigMock = $this->getMockBuilder(ScopeConfigInterface::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getValue'])
            ->getMockForAbstractClass();

        $this->errorFactoryMock = $this
            ->getMockBuilder(ErrorFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->loggerMock = $this->getMockBuilder(LoggerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->resultFactoryMock = $this->getMockBuilder(ResultFactory::class)
            ->disableOriginalConstructor()            
            ->getMock();

        $this->methodFactoryMock = $this
            ->getMockBuilder(MethodFactory::class)
            ->disableOriginalConstructor()            
            ->getMock();

        $this->helperMock = $this->getMockBuilder(Helper::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->rateRequestMock = $this->getMockBuilder(RateRequest::class)
            ->disableOriginalConstructor()            
            ->addMethods(['getPackageQty', 'getPackageValueWithDiscount', 'getBaseSubtotalWithDiscountInclTax'])
            ->getMock();

        $this->serializerMock = $this->getMockBuilder(SerializerInterface::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        //https://magento.stackexchange.com/questions/140409/magento-2-when-to-use-object-manager-in-unit-tests
        $this->model = new ShippingStandard(
            $this->scopeConfigMock,
            $this->errorFactoryMock,
            $this->loggerMock,
            $this->helperMock,
            $this->methodFactoryMock,
            $this->resultFactoryMock,
            $this->serializerMock                        
        );        
    }

    /**
     * Test validateRate
     * 
     * It's enought to test only for standard shipping as all methods 
     * (standard, express, premium) extend the same base class where
     * this method is defined
     * 
     * @access public
     * @dataProvider configDataProvider
     */
    public function testValidateRate($config, $request, $expected)
    {
        $testedMethod = new \ReflectionMethod(
            ShippingStandard::class,
            'validateRate'
        );
        $testedMethod->setAccessible(true);

        $withArgs = $returnValues = [];
        foreach ($config as $key => $value) {
            $withArgs[] = ['carriers/shirtplatform_standard/' . $key, $this->anything(), $this->anything()];
            $returnValues[] = $value;            
        }

        $this->scopeConfigMock->expects($this->any())
                ->method('getValue')
                ->withConsecutive(...$withArgs)
                ->willReturnOnConsecutiveCalls(...$returnValues);              
        
        foreach ($request as $key => $value) {
            $this->rateRequestMock->expects($this->any())
                ->method($key)
                ->willReturn($value);
        }

        //https://stackoverflow.com/questions/42094809/how-to-unit-test-protected-methods
        $result = $testedMethod->invokeArgs($this->model, [$this->rateRequestMock]);
        $this->assertEquals($expected, $result);
    }

    /**
     * Configuration data for the test
     * 
     * @access public
     * @return array
     */
    public function configDataProvider()
    {
        return [
            [
                'config' => ['min_qty' => null, 'max_qty' => null, 'min_subtotal' => null, 'max_subtotal' => null, 'subtotal_including_tax' => 0],
                'request' => ['getPackageQty' => 3, 'getPackageValueWithDiscount' => 20, 'getBaseSubtotalWithDiscountInclTax' => 24],
                'expected' => true
            ],
            [
                'config' => ['min_qty' => 5, 'max_qty' => null, 'min_subtotal' => null, 'max_subtotal' => null, 'subtotal_including_tax' => 0],
                'request' => ['getPackageQty' => 3, 'getPackageValueWithDiscount' => 20, 'getBaseSubtotalWithDiscountInclTax' => 24],
                'expected' => false
            ],
            [
                'config' => ['min_qty' => null, 'max_qty' => 5, 'min_subtotal' => null, 'max_subtotal' => null, 'subtotal_including_tax' => 0],
                'request' => ['getPackageQty' => 8, 'getPackageValueWithDiscount' => 20, 'getBaseSubtotalWithDiscountInclTax' => 24],
                'expected' => false
            ],
            [
                'config' => ['min_qty' => 1, 'max_qty' => 10, 'min_subtotal' => 10, 'max_subtotal' => 50, 'subtotal_including_tax' => 0],
                'request' => ['getPackageQty' => 3, 'getPackageValueWithDiscount' => 20, 'getBaseSubtotalWithDiscountInclTax' => 24],
                'expected' => true
            ],
            [
                'config' => ['min_qty' => 1, 'max_qty' => 10, 'min_subtotal' => 22, 'max_subtotal' => null, 'subtotal_including_tax' => 0],
                'request' => ['getPackageQty' => 3, 'getPackageValueWithDiscount' => 20, 'getBaseSubtotalWithDiscountInclTax' => 24],
                'expected' => false
            ],
            [
                'config' => ['min_qty' => 1, 'max_qty' => 10, 'min_subtotal' => 22, 'max_subtotal' => null, 'subtotal_including_tax' => 1],
                'request' => ['getPackageQty' => 3, 'getPackageValueWithDiscount' => 20, 'getBaseSubtotalWithDiscountInclTax' => 24],
                'expected' => true
            ],
            [
                'config' => ['min_qty' => null, 'max_qty' => null, 'min_subtotal' => 22, 'max_subtotal' => null, 'subtotal_including_tax' => 0],
                'request' => ['getPackageQty' => 3, 'getPackageValueWithDiscount' => 20, 'getBaseSubtotalWithDiscountInclTax' => 24],
                'expected' => false
            ],
            [
                'config' => ['min_qty' => null, 'max_qty' => null, 'min_subtotal' => 22, 'max_subtotal' => null, 'subtotal_including_tax' => 1],
                'request' => ['getPackageQty' => 3, 'getPackageValueWithDiscount' => 20, 'getBaseSubtotalWithDiscountInclTax' => 24],
                'expected' => true
            ],
            [
                'config' => ['min_qty' => null, 'max_qty' => null, 'min_subtotal' => null, 'max_subtotal' => 20, 'subtotal_including_tax' => 0],
                'request' => ['getPackageQty' => 3, 'getPackageValueWithDiscount' => 20, 'getBaseSubtotalWithDiscountInclTax' => 24],
                'expected' => true
            ],
            [
                'config' => ['min_qty' => null, 'max_qty' => null, 'min_subtotal' => null, 'max_subtotal' => 19.99, 'subtotal_including_tax' => 0],
                'request' => ['getPackageQty' => 3, 'getPackageValueWithDiscount' => 20, 'getBaseSubtotalWithDiscountInclTax' => 24],
                'expected' => false
            ],
            [
                'config' => ['min_qty' => null, 'max_qty' => null, 'min_subtotal' => null, 'max_subtotal' => 20, 'subtotal_including_tax' => 1],
                'request' => ['getPackageQty' => 3, 'getPackageValueWithDiscount' => 20, 'getBaseSubtotalWithDiscountInclTax' => 24],
                'expected' => false
            ],
            [
                'config' => ['min_qty' => null, 'max_qty' => null, 'min_subtotal' => null, 'max_subtotal' => 23.99, 'subtotal_including_tax' => 1],
                'request' => ['getPackageQty' => 3, 'getPackageValueWithDiscount' => 20, 'getBaseSubtotalWithDiscountInclTax' => 24],
                'expected' => false
            ]
        ];
    }
}
