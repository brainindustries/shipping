<?php
namespace Shirtplatform\Shipping\Model\Carrier;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\Method;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\Result;
use Magento\Shipping\Model\Rate\ResultFactory;
use Psr\Log\LoggerInterface;
use Shirtplatform\Shipping\Helper\Data as Helper;

class DefaultCarrier extends AbstractCarrier implements CarrierInterface
{
    /**
     * @var string
     */
    protected $_code = 'shirtplatform_default';

    /**
     * @var Helper
     */
    protected $_helper;

    /**
     * @var MethodFactory
     */
    protected $_rateMethodFactory;

    /**
     * @var ResultFactory
     */
    protected $_rateResultFactory;

    /**
     * @var SerializerInterface
     */
    protected $_serializer;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ErrorFactory $rateErrorFactory
     * @param LoggerInterface $logger
     * @param Helper $helper
     * @param MethodFactory $rateMethodFactory
     * @param ResultFactory $rateResultFactory
     * @param SerializerInterface $serializer
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        Helper $helper,
        MethodFactory $rateMethodFactory,
        ResultFactory $rateResultFactory,
        SerializerInterface $serializer,
        array $data = []
    ) {
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
        $this->_helper = $helper;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->_rateResultFactory = $rateResultFactory;
        $this->_serializer = $serializer;
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return [$this->_code => $this->getConfigData('name')];
    }

    /**
     * @param RateRequest $request
     * @return bool|Result
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active') || !$this->validateRate($request)){
            return false;
        }

        /** @var Result $result */
        $result = $this->_rateResultFactory->create();

        /** @var Method $method */
        $method = $this->_rateMethodFactory->create();

        $method->setCarrier($this->_code);
        $method->setCarrierTitle($this->getConfigData('title'));

        $method->setMethod($this->_code);
        $method->setMethodTitle($this->getConfigData('name'));

        $largeOrderPrefix = $this->_helper->isLargeOrder($request->getPackageValue(), $request->getPackageQty()) ? 'large_order_' : '';
        $description = $this->getConfigData($largeOrderPrefix .'description') ?: '';
        $method->setDescription($description);

        $shippingPrice = $this->getConfigData('use_dynamic_price') ? $this->getDynamicPrice($request) : $this->getConfigData('price');

        $amount = $this->getFinalPriceWithHandlingFee($shippingPrice);
        $method->setPrice($amount);
        $method->setCost($amount);

        $result->append($method);

        return $result;
    }

    /**
     * Check if method is available based on quantity and subtotal
     * 
     * @access protected
     * @param RateRequest $request
     * @return bool
     */
    protected function validateRate(RateRequest $request) {
        $minQty = $this->getConfigData('min_qty');
        $maxQty = $this->getConfigData('max_qty');
        $minSubtotal = $this->getConfigData('min_subtotal');
        $maxSubtotal = $this->getConfigData('max_subtotal');
        $subtotalInclTax = $this->getConfigData('subtotal_including_tax');

        if ($minQty !== null && $request->getPackageQty() < $minQty) {
            return false;
        }
        if ($maxQty !== null && $request->getPackageQty() > $maxQty) {
            return false;
        }

        $subtotal = $request->getPackageValueWithDiscount();
        if ($subtotalInclTax && $request->getBaseSubtotalWithDiscountInclTax()) {
            $subtotal = $request->getBaseSubtotalWithDiscountInclTax();
        }

        if ($minSubtotal !== null && $subtotal < $minSubtotal) {
            return false;
        }
        if ($maxSubtotal !== null && $subtotal > $maxSubtotal) {
            return false;
        }

        return true;
    }

    /**
     * @param RateRequest $request
     */
    private function getDynamicPrice($request)
    {
        $dynamicPrice = $this->getConfigData('dynamic_price');
        $dynamicPrice = $this->_serializer->unserialize($dynamicPrice);

        array_multisort(
            array_column($dynamicPrice, 'min_cart_price'),
            SORT_DESC,
            $dynamicPrice
        );

        $price = $request->getPackageValue();

        foreach ($dynamicPrice as $dynamicPriceItem) {
            if ($price >= $dynamicPriceItem['min_cart_price']) {
                return $dynamicPriceItem['price'];
            }
        }
    }
}
