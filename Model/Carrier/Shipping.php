<?php

namespace Shirtplatform\Shipping\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;

/**
 * Class Shipping
 * @package Shirtplatform\Shipping\Model\Carrier
 */
class Shipping extends AbstractCarrier implements CarrierInterface {

    /** @var string */
    const CARRIER_CODE = 'shirtplatformShipping';

    /** @var string */
    const ZASIELKOVNA_METHOD_NAME = 'shirtplatformShipping_23';

    /** @var string */
    const SHIPCLOUD_DHL_STANDARD = 'shirtplatformShipping_1';

    /** @var string */
    protected $_code = self::CARRIER_CODE;

    /** @var \Magento\Shipping\Model\Rate\ResultFactory */
    protected $_rateResultFactory;

    /** @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory */
    protected $_rateMethodFactory;    

    /**
     * @var \Magento\Framework\DataObjectFactory $dataObjectFactory
     */
    private $_dataObjectFactory;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;

    /** @var \Shirtplatform\Shipping\Helper\Data */
    private $shippingDataHelper;

    /**
     * @var \Magento\Sales\Api\ShipmentTrackRepositoryInterface
     */
    private $_trackRepository;

    /**
     * Shipping constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\DataObjectFactory $dataObjectFactory
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory     
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Shirtplatform\Shipping\Helper\Data $shippingDataHelper
     * @param \Magento\Sales\Api\ShipmentTrackRepositoryInterface $trackRepository
     * @param array $data
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
                                \Psr\Log\LoggerInterface $logger,
                                \Magento\Framework\DataObjectFactory $dataObjectFactory,
                                \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
                                \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,                                                             
                                \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
                                \Shirtplatform\Shipping\Helper\Data $shippingDataHelper,
                                \Magento\Sales\Api\ShipmentTrackRepositoryInterface $trackRepository,
                                array $data = []
    ) {
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
        $this->_dataObjectFactory = $dataObjectFactory;
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;        
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->shippingDataHelper = $shippingDataHelper;
        $this->_trackRepository = $trackRepository;        
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getAllowedMethods() {
        $result = [];
        foreach ($this->shippingDataHelper->getShippingModuleProviders() as $moduleProvider) {
            $result[$moduleProvider->id] = $moduleProvider->name;
        }
        return $result;
    }

    /**
     * @param RateRequest $request
     * @return bool|\Magento\Shipping\Model\Rate\Result
     * @throws \Exception
     */
    public function collectRates(RateRequest $request) {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        \Magento\Framework\Profiler::start('getPlatformShippingRates');
        $platformShippingMethods = $this->shippingDataHelper->getPlatformShippingMethods($request);
        \Magento\Framework\Profiler::stop('getPlatformShippingRates');

        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->_rateResultFactory->create();        
                        
        $subtotalWithTaxAndDiscount = $request->getBaseSubtotalWithDiscountInclTax();
        $zasielkovnaMaxOrderAmount = $this->getConfigData('zasielkovna_max_order_amount');
        if ($zasielkovnaMaxOrderAmount) {
            $zasielkovnaMaxOrderAmount = str_replace(',', '.', $zasielkovnaMaxOrderAmount);        
        }
        
        foreach ($platformShippingMethods as $shippingMethod) {
            $title = 'shirtplatformShipping_' . $shippingMethod['uniqId'];

            if ($title == self::ZASIELKOVNA_METHOD_NAME and $zasielkovnaMaxOrderAmount and $zasielkovnaMaxOrderAmount < $subtotalWithTaxAndDiscount) {
                continue;
            }

            /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
            $method = $this->_rateMethodFactory->create();
            $method->setCarrier($this->_code);
            $method->setCarrierTitle($shippingMethod['carrierTitle']);
            $method->setMethod($shippingMethod['uniqId']);
            $method->setMethodTitle($shippingMethod['methodTitle']);
            $method->setUsedInAdditionalLogic($shippingMethod['usedInAdditionalLogic']);

            $method->setPrice($shippingMethod['price']);
            $method->setCost($shippingMethod['price']);
            $result->append($method);            
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function isTrackingAvailable() {
        return true;
    }

    /**
     * Get tracking info
     * 
     * @param string $trackNumber
     * @access public
     * @return \Magento\Framework\DataObject|\Magento\Sales\Model\Order\Shipment\Track
     */
    public function getTrackingInfo($trackNumber) {        
        $result = $this->_dataObjectFactory->create();
        $searchCriteria = $this->_searchCriteriaBuilder->addFilter('track_number', $trackNumber)->create();
        $tracks = $this->_trackRepository->getList($searchCriteria)->getItems();        
        $result->setTrackNumber('');
        
        if (!empty($tracks)) {
            $result = reset($tracks);                                    
            $result->setTracking($result->getTrackNumber());            
        }
        
        return $result;        
    }
}
