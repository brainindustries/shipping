<?php
namespace Shirtplatform\Shipping\Model\Carrier;

use Magento\Shipping\Model\Carrier\CarrierInterface;

class ShippingBasic extends DefaultCarrier implements CarrierInterface
{
    /**
     * @var string
     */
    protected $_code = 'shirtplatform_basic';
}
