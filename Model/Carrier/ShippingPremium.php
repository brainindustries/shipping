<?php
namespace Shirtplatform\Shipping\Model\Carrier;

use Magento\Shipping\Model\Carrier\CarrierInterface;

class ShippingPremium extends DefaultCarrier implements CarrierInterface
{
    /**
     * @var string
     */
    protected $_code = 'shirtplatform_premium';
}
